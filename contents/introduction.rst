============
Introduction
============

Software Defined Vehicle
------------------------
Mobility undergoes a significant digital transition towards becoming connected, autonomous, shared and electric (CASE). This transformation bases majorly on Software Technologies and the Software Defined Vehicle is characterized by managing its functionality and its operations to a large extent by Software throughout its entire lifecycle. 

Centralization of Compute
-------------------------
With a software-centric focus, the electronic architectures of vehicles transform towards greater centralization of compute which is happening due to both technical and business reasons. 
This trend in consolidation of compute in automotive allows for improvements such as the reduction of wiring complexity and weight amongst other factors. These improvements allow for supply chain and manufacturing optimisations with the promise of easier and less expensive product development. 

New Business Models
-------------------
The move towards Software Defined Vehicles enables an OEM to enhance the vehicle’s capabilities over time as new features are expressed in software that can be delivered over the air. This will allow for enhanced business models whereby the end users can purchase additional features after the vehicle has left the dealership.

Separation of Hardware and Software
-----------------------------------
With vehicle functions getting updated or enhanced throughout the entire lifecycle of a vehicle, it becomes essential to deploy identical Software Components to different vehicle computer variants and therefore achieve a separation of Hardware and Software. 

Virtual Development and Validation
----------------------------------
To shorten the time-to-market for new Software Functions and allow for scalability of Software Validation with the number of vehicle and therefore hardware variants, use of Virtual Development and Virtual Validation becomes essential. 
Fast and Frequent Software Deployments 
With the penetration of vehicles being connected to the digital consumer ecosystem, the need for fast and frequent Software Deployments is majorly driven by Cyber Security requirements, which currently get increasingly important. In addition to technical and business aspects, legal standards have evolved significantly. 
Additionally the lifecycle of hardware and software will be decoupled and the latest version of a software might be installed on vehicles, which are already in operation for several years. 

Impacts
-------
There are many benefits to the industry migrating to a Software Defined Vehicle model, but there are differentiating and non-differentiating challenges with this migration. 
The first issue is that traditionally the vehicle has been seen as an embedded platform, where the software has been hardware-dependent to a large extent. This means that the software for one system may not be easily transferred to another without potential large up-front engineering costs. In a Software Defined world, anything that adds barriers such dependencies add to overall development costs reduces attractiveness in a competitive market. 
The second issue arises with the increased complexity of the centralized compute modules. These may include heterogeneous compute islands for application, real-time and microcontroller based workloads, each needing to support different profiles ranging from QM to ASIL-D, from low to high security and from best-effort to hard

real-time profiles, including combinations thereof. The configuration and deployment of workloads to these SoC’s has the potential to become very system specific, again reducing the ability for workloads to easily migrate from one system to another. 
These two primary complexity angles are potential barriers that will challenge the pace at which the industry migrates towards the Software Defined Vehicle. Increasing the pace of migration to a Software Defined Vehicle in automotive will rely on industry agreeing on open standards and methodologies to help manage and mitigate these challenges. The SOAFEE Special Interest Group (SIG) is addressing these challenges with a standardized system architecture specification. This specification will mature and evolve over time as open standards and best known methodologies evolve.
