===================
SOAFEE Architecture
===================

This document contains the SOAFEE architecture in `reStructuredText <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_ format.

In order to view the documentation locally, please follow the `Installing Sphinx <https://www.sphinx-doc.org/en/master/usage/installation.html>`_ instructions.

Once installed, you can run `make html` to build the documentation locally.
